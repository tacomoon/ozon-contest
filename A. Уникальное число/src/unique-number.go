package main

import (
	"bufio"
	"os"
)

func main() {
	input, _ := os.Open("./unique-number/input.txt")
	defer input.Close()

	m := make(map[string]int)

	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		number := scanner.Text()
		m[number] += 1
	}

	output, _ := os.Create("./unique-number/output.txt")
	defer output.Close()

	for number, occurrences := range m {
		if occurrences == 1 {
			output.WriteString(number)
			break
		}
	}
}