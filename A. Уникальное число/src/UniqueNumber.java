import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UniqueNumber {
    public static void main(String[] args) throws FileNotFoundException {
        final HashMap<String, Integer> map = new HashMap<>();

        final File file = new File("input-201.txt");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                map.compute(line, (key, old) -> old == null ? 1 : old + 1);
            }
        } catch (IOException ignored) {
        }

        map.entrySet().stream()
                .filter(entry -> entry.getValue() == 1)
                .findFirst()
                .map(Map.Entry::getKey)
                .ifPresent(System.out::println);
    }
}
