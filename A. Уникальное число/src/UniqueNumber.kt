fun main(args: Array<String>) {
    val map = mutableMapOf<String, Int>()

    while (true) {
        val line = readLine() ?: break
        map.compute(line) { _, old -> if (old == null) 1 else old + 1 }
    }

    val result = map.entries.asSequence()
            .first { entry -> entry.value == 1 }
            .value

    println(result)
}