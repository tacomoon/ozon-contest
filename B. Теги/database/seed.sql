CREATE TABLE tags (
    id   SERIAL NOT NULL PRIMARY KEY,
    name TEXT   NOT NULL
);

CREATE TABLE goods (
    id   SERIAL NOT NULL PRIMARY KEY,
    name TEXT   NOT NULL
);

CREATE TABLE tags_goods (
    tag_id   INT NOT NULL REFERENCES tags(id),
    goods_id INT NOT NULL REFERENCES goods(id),
    PRIMARY KEY (tag_id, goods_id)
);

INSERT INTO tags (name)
VALUES ('tag1'),
    ('tag2'),
    ('tag3');

INSERT INTO goods(name)
VALUES ('goods1'),
    ('goods2'),
    ('goods3');

INSERT INTO tags_goods (tag_id, goods_id)
VALUES (1, 1),
    (1, 2),
    (1, 3),
    (2, 1),
    (2, 3),
    (3, 1),
    (3, 3),
    (3, 2);