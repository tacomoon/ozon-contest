SELECT g.id, g.name
FROM goods g
WHERE g.id IN (
    SELECT tg.goods_id
    FROM tags_goods tg
    GROUP BY tg.goods_id
    HAVING count(*) = (SELECT count(*) FROM tags)
);