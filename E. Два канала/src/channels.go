package main

import (
	"fmt"
	"math/rand"
	"time"
)

func writeToChannel(name string, channel chan<- int, n int) {
	fmt.Printf("%4s: generating values\n", name)
	for i := 1; i <= n; i++ {
		value := rand.Intn(100)
		fmt.Printf("%4s: writing %d\n", name, value)

		channel <- value
	}

	fmt.Printf("%4s: closing channel\n", name)
	close(channel)
}

func main() {
	n := 5

	in1 := make(chan int)
	in2 := make(chan int)
	out := make(chan int)

	f := func(x int) int {
		fmt.Printf("func: x=%d, performing...\n", x)
		time.Sleep(time.Duration(rand.Intn(10)) * time.Second)

		return x
	}

	go writeToChannel("in1", in1, n)
	go writeToChannel("in2", in2, n)

	Merge2Channels(f, in1, in2, out, n)

	fmt.Println("main: merge started")

	for i := 0; i < n; i++ {
		fmt.Printf(" out: %d\n", <-out)
	}
}

func run(f func(int) int, arg int, channel chan<- int) {
	channel <- f(arg)
	close(channel)
}

func Merge2Channels(f func(int) int, in1 <-chan int, in2 <-chan int, out chan<- int, n int) {
	go func() {
		for i := 0; i < n; i++ {
			x1 := <-in1
			x2 := <-in2

			result1 := make(chan int, 1)
			result2 := make(chan int, 2)
			go run(f, x1, result1)
			go run(f, x2, result2)

			out <- <-result1 + <-result2
		}
	}()
}
