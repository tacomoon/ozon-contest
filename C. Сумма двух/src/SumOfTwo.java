import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;

public class SumOfTwo {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new File("input.txt"));

        int sum = scanner.nextInt();

        HashSet<Integer> checked = new HashSet<>();
        while (scanner.hasNext()) {
            int x = scanner.nextInt();
            int y = sum - x;

            if (y >= 0 && checked.contains(y)) {
                print("1");
                return;
            }
            checked.add(x);
        }

        print("0");
    }

    private static void print(String text) throws IOException {
        File output = new File("output.txt");
        BufferedWriter writer = new BufferedWriter(new FileWriter(output));

        writer.write(text);
        writer.close();
    }
}