#include <iostream>
#include <fstream>
#include <unordered_set>

using namespace std;

int main() {
    fstream input("../input.txt", ios_base::in);
    fstream output("../output.txt", ios_base::out);

    int sum;
    input >> sum;

    int x, y;
    unordered_set<int> set;
    while (input >> x) {
        y = sum - x;

        if (y > 0 && set.find(y) != set.end()) {
            output << "1";
            return 0;
        }

        set.insert(x);
    }

    output << "0";
    return 0;
}