package main

import (
	"bufio"
	"math"
	"os"
)

func readInt(reader *bufio.Reader, delim byte) (int, error) {
	bytes, err := reader.ReadBytes(delim)
	if err != nil {
		return 0, err
	}

	var sum = 0
	for i := 0; i <= len(bytes)-2; i++ {
		sum += (int)(math.Pow(10, float64(i))) * (int)(bytes[i]^byte(48))
	}

	return sum, nil
}

func main() {
	input, _ := os.Open("input.txt")
	defer input.Close()

	reader := bufio.NewReader(input)

	sum, _ := readInt(reader, '\n')

	m := make(map[int]bool)
	for {
		x, err := readInt(reader, ' ')
		if err != nil {
			break
		}

		y := sum - x
		if y >= 0 {
			if _, found := m[y]; found {
				write("1")
				return
			}
		}
		m[x] = true
	}

	write("0")
}

func write(text string) {
	output, _ := os.Create("output.txt")
	defer output.Close()

	output.WriteString(text)
}