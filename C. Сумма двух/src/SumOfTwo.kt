import java.io.File

fun main() {
    val reader = File("input.txt").bufferedReader()
    val writer = File("output.txt").bufferedWriter()

    val sum = reader.readLine()!!
            .toInt()
    val numbers = reader.readLine()!!
            .split(" ")
            .map(String::toLong)

    val checked = ArrayList<Long>(numbers.size)
    for (number in numbers) {
        val y = sum - number

        if (checked.any { it == y }) {
            return writer.use { it.write("1") }
        } else {
            checked.add(number)
        }
    }

    writer.use { it.write("0") }
}