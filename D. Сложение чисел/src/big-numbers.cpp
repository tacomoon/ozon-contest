#include <iostream>

using namespace std;

void reverse(string &str) {
    int len = str.length();
    for (int i = 0; i < len / 2; ++i) {
        swap(str[i], str[len - i - 1]);
    }
}

string add(string &longer, string &shorter) {
    string result;

    int carry = 0;
    int shift = '0';

    int i = longer.size() - 1;
    for (int j = shorter.size() - 1; j >= 0; i--, j--) {
        int l = longer.at(i) - shift;
        int s = shorter.at(j) - shift;

        int sum = l + s + carry;
        carry = sum / 10;
        result += (char) (sum % 10 + shift);
    }

    for (; i >= 0; i--) {
        int l = longer.at(i) - shift;

        int sum = l + carry;
        carry = sum / 10;
        result += (char) (sum % 10 + shift);
    }

    if (carry > 0) {
        result += (char) (carry + shift);
    }

    reverse(result);

    return result;
}

int main() {
    string a, b;
    cin >> a;
    cin >> b;

    string result;
    if (a.size() > b.size()) {
        result = add(a, b);
    } else {
        result = add(b, a);
    }

    cout << result;

    return 0;
}