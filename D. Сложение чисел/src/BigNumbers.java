import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class BigNumbers {
    private static final int shift = '0';

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out));

        String[] split = reader.readLine().split(" ");
        String a = split[0];
        String b = split[1];

        String result = add(a, b);

        writer.write(result);
        writer.close();
    }

    private static String add(String a, String b) {
        String longer, shorter;
        if (a.length() > b.length()) {
            longer = a;
            shorter = b;
        } else {
            longer = b;
            shorter = a;
        }

        StringBuilder result = new StringBuilder();

        int carry = 0;
        int lindex = longer.length() - 1;

        for (int sindex = shorter.length() - 1; sindex >= 0; lindex--, sindex--) {
            int lnumber = longer.charAt(lindex) - shift;
            int snumber = shorter.charAt(sindex) - shift;

            int sum = lnumber + snumber + carry;

            carry = sum / 10;
            result.append((char) (sum % 10 + shift));
        }

        for (; lindex >= 0; lindex--) {
            int lnumber = longer.charAt(lindex) - shift;
            int sum = lnumber + carry;

            carry = sum / 10;
            result.append((char) (sum % 10 + shift));
        }

        if (carry > 0) {
            result.append((char) (carry + shift));
        }

        return result.reverse().toString();
    }
}
